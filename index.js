const express = require('express')
const bp = require('body-parser')
const cors = require('cors')
const routes = require('./src/routes')


const server = express()

server.use(cors())
server.use(express.json())
server.use(express.json())
server.use(bp.urlencoded({extended:true}))
server.use(routes)
server.get("/",(req,res)=>{
    res.json({saudacoes : "ola"})
})

server.listen(3002, ()=>{
    console.log("Server running on 3002")
})