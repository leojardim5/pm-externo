const axios = require('axios');
const getCiclistaInfoSimulada = require('../model/Ciclista')
const { getCartaoByIdCiclista, alterarDadosCartao } = require('../model/CartaoDeCredito')


const apisExternas = {
    getCiclista: async(ciclista) => {
        try {
            return getCiclistaInfoSimulada(ciclista)
        } catch (error) {
            console.log(error);
            return "Dados inválidos";
        }
    },
    getCartaoDeCredito: async(ciclista) => {
        try {
            await getCartaoByIdCiclista(ciclista)
        } catch (error) {
            console.log(error);
            return "Dados inválidos";;
        }
    },

    putCartaoDeCredito: async(infoCartao) => {
        try {
            alterarDadosCartao(infoCartao)
        } catch (error) {
            console.log(error)
        }
    }
};

module.exports = apisExternas;