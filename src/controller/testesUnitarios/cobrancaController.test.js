const {getCartaoByIdCiclista,cartoesModel} = require('../../model/CartaoDeCredito'); // ajuste o caminho conforme necessário

function runTests() {

    let resultado;

    resultado = getCartaoByIdCiclista(1);
    console.assert(
        JSON.stringify(resultado) === JSON.stringify({
            "IdDoCiclista": 1,
            "nomeTitular": "João Silva",
            "numero": "1111222233334444",
            "validade": "12/25",
            "cvv": "123"
        }),
        'Erro: Teste 1 falhou'
    );

    resultado = getCartaoByIdCiclista(99);
    console.assert(
        resultado === null,
        'Erro: Teste 2 falhou'
    );

    resultado = getCartaoByIdCiclista(3);
    console.assert(
        JSON.stringify(resultado) === JSON.stringify({
            "IdDoCiclista": 3,
            "nomeTitular": "Carlos Gonzalez",
            "numero": "9999888877776666",
            "validade": "10/23",
            "cvv": "789"
        }),
        'Erro: Teste 3 falhou'
    );

    resultado = getCartaoByIdCiclista(1);
    console.assert(
        JSON.stringify(resultado) === JSON.stringify({
            "IdDoCiclista": 1,
            "nomeTitular": "João Silva",
            "numero": "1111222233334444",
            "validade": "12/25",
            "cvv": "123"
        }),
        'Erro: Teste 4 falhou'
    );

    console.log('Todos os testes passaram!');
}

runTests();
