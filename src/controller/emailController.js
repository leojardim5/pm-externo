const nodemailer = require('nodemailer');
const regExp = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
const novoEmail = require('../model/Email');
const { transporter } = require('../nodeMailerConfig');
const axios = require('axios');
const { getCiclistaInfo } = require("../model/Ciclista");
const { obterListaCobrancas } = require("../model/Cobranca");
const { cartoesModel } = require('../model/CartaoDeCredito');
const accessToken = 'EAAAECBY7OWaI-noJKMoDu_wlRUXx4yI08ICQ3rX6DxfPSzjvG398fs5B8B4KxT2'


const emailController = {
    enviarEmail: async (req, res) => {
        const { mensagem, email, assunto } = req.body;

        try {
            if (!email || !mensagem) {
                return res.status(404).json({ msg: "Erro, email não existe" });
            }

            if (!regExp.test(email)) {
                return res.status(422).json({ msg: "Erro, email com formato inválido" });
            }

            const mailOptions = {
                from: "vadebikke@gmail.com",
                to: email,
                subject: assunto,
                text: mensagem,
            };
            
            await transporter.sendMail(mailOptions);

            console.log(assunto);
            if (res) {
                res.status(200).json({ msg: "Sucesso, e-mail enviado" });
            } else {
                console.error("O objeto 'res' é undefined");
            }
        } catch (erro) {
            console.error(erro);
            if (res) {
                res.status(422).json({ msg: "Erro na requisição",erro });
            } else {
                console.error("O objeto 'res' é undefined");
            }
        }
    },
};

module.exports = emailController;



