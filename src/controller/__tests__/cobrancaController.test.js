
const axios = require('axios');
const crypto = require('crypto');
const { processaCobrancasEmFila, realizarCobranca,pagamentoComCartao,getCobranca } = require('../cobrancaController');
const {obterListaCobrancas, obterCobranca,inserirCobranca} = require('../../funcSimplesModel/serviceCobranca')
const { enviarEmail } = require('../emailController');

jest.mock('axios');
jest.mock('crypto');

jest.mock('../cobrancaController', () => ({
    processaCobrancasEmFila: jest.fn(),
    realizarCobranca: jest.fn(),
    pagamentoComCartao: jest.fn(),
    getCobranca: jest.fn()
}));

jest.mock('../emailController', () => ({
    enviarEmail: jest.fn(),
}));

jest.mock('../../funcSimplesModel/serviceCobranca', ()=>({
    obterListaCobrancas: jest.fn(),
    obterCobranca: jest.fn(),
    inserirCobranca: jest.fn()
}));


describe('pagamentoComCartao', () => {
    beforeEach(() => {
        crypto.randomUUID.mockReturnValue('id');
    });

    it('sucesso ao processar o pagamento', async () => {
        const respostaMockada = { data: 'Pagamento aprovado' };
        axios.post.mockResolvedValue(respostaMockada);

        const req = { body: { valor: 1000 } };
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };

        const responseApi = await pagamentoComCartao(req, res);

        expect(responseApi.status).toBe(200);
        expect(responseApi.json).toEqual({ msg: "Pagamento processado com sucesso", response: respostaMockada });
    });

    it('erro no processamento do pagamento', async () => {
        const respostaMockadaErrada = new Error('Falha no pagamento');
        axios.post.mockRejectedValue(respostaMockadaErrada);

        const req = { body: { valor: 1000 } };
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };

        const responseApi = await pagamentoComCartao(req, res);

        expect(responseApi.status).toBe(500);
        expect(responseApi.json).toEqual({ msg: "Erro ao processar o pagamento", error: respostaMockadaErrada });
    });
});

describe('processaCobrancasEmFila', () => {
    let req;
    let res;

    beforeEach(() => {
        req = {};
        res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };
    });

    it('Cobranca processadas com sucesso', async () => {
        const listaCobrancas = [
            { status: "Pendente", valor: 100, ciclista: { nome: "João", email: "joao@example.com" }, horaSolicitacao: "2024-08-16" },
            { status: "Pendente", valor: 200, ciclista: { nome: "Maria", email: "maria@example.com" }, horaSolicitacao: "2024-08-16" },
        ];

        obterListaCobrancas.mockReturnValue(listaCobrancas);
        realizarCobranca.mockResolvedValue({ status: 200 });
        enviarEmail.mockResolvedValue({ status: 200 });

        await processaCobrancasEmFila(req, res);

        expect(realizarCobranca).toHaveBeenCalledTimes(listaCobrancas.length);
        expect(enviarEmail).toHaveBeenCalledTimes(listaCobrancas.length);
        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledWith({ msg: "Foram pagas cobranças atrasadas com sucesso" });
    });

    it('deve retornar erro se alguma cobrança falhar', async () => {
        const listaCobrancas = [
            { status: "Pendente", valor: 100, ciclista: { nome: "João", email: "joao@example.com" }, horaSolicitacao: "2024-08-16" },
            { status: "Pendente", valor: 200, ciclista: { nome: "Maria", email: "maria@example.com" }, horaSolicitacao: "2024-08-16" },
        ];

        obterListaCobrancas.mockReturnValue(listaCobrancas);
        realizarCobranca
            .mockResolvedValueOnce({ status: 200 })
            .mockRejectedValueOnce(new Error('Falha na cobrança'));

        await processaCobrancasEmFila(req, res);

        expect(realizarCobranca).toHaveBeenCalledTimes(listaCobrancas.length);
        expect(enviarEmail).toHaveBeenCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledWith({ msg: "Erro ao processar alguma cobrança na fila de cobrança" });
    });

    it('deve retornar erro se a fila inteira falhar', async () => {
        const listaCobrancas = [
            { status: "Pendente", valor: 100, ciclista: { nome: "João", email: "joao@example.com" }, horaSolicitacao: "2024-08-16" },
        ];

        obterListaCobrancas.mockReturnValue(listaCobrancas);
        realizarCobranca.mockRejectedValue(new Error('Falha na cobrança'));

        await processaCobrancasEmFila(req, res);

        expect(realizarCobranca).toHaveBeenCalledTimes(1);
        expect(enviarEmail).not.toHaveBeenCalled();
        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledWith({ msg: "Erro ao processar alguma cobrança na fila de cobrança" });
    });

    it('deve retornar erro se ocorrer uma exceção durante o processamento', async () => {
        obterListaCobrancas.mockImplementation(() => {
            throw new Error('Erro inesperado');
        });

        await processaCobrancasEmFila(req, res);

        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledWith({
            msg: "Erro ao processar a fila de cobrança",
            error: "Erro inesperado",
        });
    });
});


describe('getCobranca', () => {
    let req;
    let res;

    beforeEach(() => {
        req = { params: { id: '123' } };
        res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };
    });

    it('deve retornar a cobrança com sucesso', async () => {
        const cobrancaMockada = { id: '123', valor: 100, status: 'Pendente' };
        obterCobranca.mockResolvedValue(cobrancaMockada);

        await getCobranca(req, res);

        expect(obterCobranca).toHaveBeenCalledWith('123');
        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledWith({ msg: cobrancaMockada });
    });

    it('deve retornar erro 404 se a cobrança não for encontrada', async () => {
        obterCobranca.mockResolvedValue(null);

        await getCobranca(req, res);

        expect(obterCobranca).toHaveBeenCalledWith('123');
        expect(res.status).toHaveBeenCalledWith(404);
        expect(res.json).toHaveBeenCalledWith({ msg: "Nao encontrado" });
    });

    it('deve retornar erro 500 se ocorrer um erro ao buscar a cobrança', async () => {
        const erroMockado = new Error('Erro ao buscar cobranca');
        obterCobranca.mockRejectedValue(erroMockado);

        await getCobranca(req, res);

        expect(obterCobranca).toHaveBeenCalledWith('123');
        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledWith({ msg: "Erro ao buscar cobrança" });
    });
});