const axios = require('axios');
const getCiclistaInfo = require('../funcSimplesModel/serviceCiclista')

const { obterListaCobrancas, obterCobranca, inserirCobranca } = require("../funcSimplesModel/serviceCobranca")

const { cartoesModel } = require('../funcSimplesModel/serviceCartao');

const emailController = require('./emailController')
const regexNumeroCartao = /^\d{16}$/;
const regexValidade = /^\d{4}-\d{2}-\d{2}$/;
const accessToken = 'EAAAECBY7OWaI-noJKMoDu_wlRUXx4yI08ICQ3rX6DxfPSzjvG398fs5B8B4KxT2'
const { getCartaoDeCredito, getCiclista, putCartaoDeCredito } = require('./apiExternas');




const cobrancaController = {
    realizarCobranca: async(req, res) => {
        const tempRes = { status: () => ({ json: () => {} }) };
        const { valor, ciclista } = req.body;


        try {
            if (!valor || !ciclista) {
                return res.status(500).json({ msg: "Erro ao processar a cobrança" });
            }

            const ciclistaInfo = await getCiclista(ciclista);
            const cartaoCiclista = await getCartaoDeCredito(ciclista);


            if (ciclistaInfo == "Dados inválidos") {
                return res.status(500).json({ msg: "Erro ao processar a cobrança" });
            }

            const response = await cobrancaController.pagamentoComCartao({ body: { valor: parseInt(valor), ciclista: ciclistaInfo, cartaoCiclista } }, tempRes);


            const textEmail = `Bom dia ${ciclistaInfo.nome}, esperamos que essa mensagem lhe encontre bem. Uma cobrança de ${valor} acabou de ser quitada.`;

            await emailController.enviarEmail({
                body: {
                    mensagem: textEmail,
                    email: ciclistaInfo.email,
                    assunto: "Cobrança quitada do Mês"
                }
            }, tempRes);

            if (response && response.status == 200) {
                return { status: 200, msg: "Cobrança solicitada com sucesso!" };
            } else {
                return { status: 500, msg: "Erro ao processar a cobrança" };
            }
        } catch (error) {
            console.log(error);
            return { status: 500, msg: "Erro ao processar a cobrança" };
        }
    },

    processaCobrancasEmFila: async(req, res) => {
        const listaCobrancas = obterListaCobrancas();
        let deuerro = false;

        try {
            const promises = listaCobrancas.map(async(cobranca) => {
                if (cobranca.status === "Pendente") {
                    const tempRes = { status: () => ({ json: () => {} }) };
                    try {
                        const response = await cobrancaController.realizarCobranca({ body: { valor: cobranca.valor, ciclista: cobranca.ciclista } }, tempRes);

                        if (response && response.status === 200) {
                            cobranca.status = "Concluida";
                            const textEmail = `Bom dia ${cobranca.ciclista.nome}, esperamos que essa mensagem lhe encontre bem. Uma cobrança atrasada de ${cobranca.valor} acabou de ser quitada, referente ao dia ${cobranca.horaSolicitacao}.`;
                            await emailController.enviarEmail({
                                body: {
                                    mensagem: textEmail,
                                    email: cobranca.ciclista.email,
                                    assunto: "Cobrança atrasada quitada do Mês"
                                }
                            }, tempRes);
                        } else {
                            deuerro = true;
                        }
                    } catch (error) {
                        console.log(error);
                        deuerro = true;
                    }
                }
            });

            await Promise.all(promises);

            if (!deuerro) {
                return res.status(200).json({ msg: "Foram pagas cobranças atrasadas com sucesso" });
            } else {
                return res.status(500).json({ msg: "Erro ao processar alguma cobrança na fila de cobrança" });
            }
        } catch (error) {
            return res.status(500).json({ msg: "Erro ao processar a fila de cobrança", error: error.message });
        }
    },


    getCobranca: async(req, res) => {
        const { id } = req.params;

        try {

            const cobranca = await obterCobranca(id);
            console.log(cobranca)
                // console.log(cobranca)

            if (cobranca) {
                res.status(200).json({ msg: cobranca });
            } else {
                res.status(404).json({ msg: "Nao en contrado" });
            }
        } catch (error) {

            res.status(500).json({ msg: "Erro ao buscar cobrança" });
        }
    },

    filaCobranca: async(req, res) => {
        try {
            const { valor, ciclista } = req.body;
            if (!valor) return res.status(500).json({ msg: "Erro ao processar a cobrança" });
            const horaAtual = new Date().toISOString();
            const novaCobranca = {
                id: obterListaCobrancas.length + 1,
                status: "Pendente",
                horaSolicitacao: horaAtual,
                horaFinalizacao: null,
                valor,
                ciclista,
            };

            inserirCobranca(novaCobranca);
            return res.status(200).json({ msg: "Cobrança adicionada com sucesso", cobranca: novaCobranca });
        } catch (error) {
            return res.status(500).json({ msg: "Erro ao processar a cobrança" });
        }
    },

    alterarCartaoDeCredito: async(req, res) => {
        const tempRes = { status: () => ({ json: () => {} }) };
        const response = validaCartaoDeCredito(req.body, tempRes)
        const ciclista = getCiclistaInfo(req.body.id)

        if (response.status === 200) {
            putCartaoDeCredito(req.body);
            emailController.enviarEmail({
                body: {
                    mensagem: textEmail,
                    email: ciclista.email,
                    assunto: "Cartao alterado com sucesso"
                }
            }, tempRes)

            res.status(200).json({ msg: "Dados alterados" });
        } else {
            return res.status(422).json({ msg: "Dados Inválidos" });
        }


    },

    validaCartaoDeCredito: async(req, res) => {
        const { nomeTitular, numero, validade, cvv } = req.body;

        if (!nomeTitular || !numero || !validade || !cvv ||
            !regexNumeroCartao.test(numero) || !regexValidade.test(validade)) {
            return res.status(422).json({ msg: "Dados Inválidos" });
        }

        const url = `https://lookup.binlist.net/${numero}`;

        try {
            const response = await axios.get(url);

            if (response.status === 200) {
                res.status(200).json({ msg: "Dados validados" });
            } else {
                res.status(422).json({ msg: "Dados Inválidos" });
            }
        } catch (error) {
            res.status(422).json({ msg: "Dados Inválidos" });
        }
    },

    pagamentoComCartao: async(req, res) => {
        const { valor } = req.body;
        const dados = {
            amount_money: {
                amount: valor,
                currency: 'USD',
            },
            idempotency_key: crypto.randomUUID(),
            source_id: 'cnon:card-nonce-ok',
        };

        try {
            const response = await axios.post("https://connect.squareupsandbox.com/v2/payments", dados, {
                headers: {
                    'Square-Version': '2023-11-15',
                    'Authorization': `Bearer ${accessToken}`,
                    'Content-Type': 'application/json',
                },
            });
            res.status(200).json({ msg: "Pagamento processado com sucesso", response });
            return { status: 200, json: ({ msg: "Pagamento processado com sucesso" }) };
        } catch (error) {
            res.status(500).json({ msg: "Erro ao processar o pagamento", error });
            return { status: 500, json: ({ msg: "Erro ao processar o pagamento", error }) };
        }
    }

};


module.exports = cobrancaController;