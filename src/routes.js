
const express = require('express');
const emailController = require('./controller/emailController');
const cobrancaController = require('./controller/cobrancaController');
const routes = express.Router();

routes.post('/validaCartaoDeCredito', (req, res) => cobrancaController.validaCartaoDeCredito(req, res));
routes.post('/filacobranca', (req, res) => cobrancaController.filaCobranca(req, res));
routes.get('/getCobranca/:id', (req, res) => cobrancaController.getCobranca(req, res));
routes.post('/processaEmFila', (req, res) => cobrancaController.processaCobrancasEmFila(req, res));
routes.post('/cobranca', (req, res) => cobrancaController.realizarCobranca(req, res));
routes.post('/enviaremail', (req, res) => emailController.enviarEmail(req, res));

module.exports = routes;



