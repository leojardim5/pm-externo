const cartoesModel = require("../model/CartaoDeCredito");

const getCartaoByIdCiclista = (idCiclista) => {
    const cartao = cartoesModel.find(cartao => cartao.IdDoCiclista === idCiclista);
    return cartao || null;
};

const alterarDadosCartao = (info) => {
    const { idCiclista } = info
    const index = cartoesModel.findIndex(cartao => cartao.IdDoCiclista == idCiclista)
    cartoesModel[index] = {...cartoesModel[index], ...info };
}

module.exports = { getCartaoByIdCiclista, alterarDadosCartao }