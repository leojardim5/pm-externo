const filaCobrancasBd = require("../model/Cobranca");

function obterListaCobrancas() {
    return filaCobrancasBd;
}

function obterCobranca(idCobranca) {

    return filaCobrancasBd.find(cobranca => cobranca.id === parseInt(idCobranca));
}

function inserirCobranca(novaCobranca) {
    filaCobrancasBd.push(novaCobranca);
}

module.exports = { obterCobranca, obterListaCobrancas, inserirCobranca }