const filaCobrancasBd = [{
        "id": 1,
        "status": "Pendente",
        "horaSolicitacao": "2023-11-14T13:44:44.391Z",
        "horaFinalizacao ": null,
        "valor": 10,
        "ciclista": "2"
    },
    {
        "id": 2,
        "status": "Concluída",
        "horaSolicitacao": "2023-11-14T14:15:00.000Z",
        "horaFinalizacao ": "2023-11-14T14:20:30.000Z",
        "valor": 15,
        "ciclista": "1"
    },
    {
        "id": 3,
        "status": "Pendente",
        "horaSolicitacao": "2023-11-14T15:30:00.000Z",
        "horaFinalizacao ": null,
        "valor": 12,
        "ciclista": 1
    },
    {
        "id": 4,
        "status": "Concluída",
        "horaSolicitacao": "2023-11-14T16:10:00.000Z",
        "horaFinalizacao ": "2023-11-14T16:25:45.000Z",
        "valor": 18,
        "ciclista": 2
    },
    {
        "id": 5,
        "status": "Pendente",
        "horaSolicitacao": "2023-11-14T17:45:00.000Z",
        "horaFinalizacao ": null,
        "valor": 14,
        "ciclista": 1
    },

    {
        "id": 6,
        "status": "Pendente",
        "horaSolicitacao": "2023-11-14T18:30:00.000Z",
        "horaFinalizacao ": null,
        "valor": 22,
        "ciclista": 2
    },
    {
        "id": 7,
        "status": "Concluída",
        "horaSolicitacao": "2023-11-15T10:30:00.000Z",
        "horaFinalizacao ": "2023-11-15T10:45:15.000Z",
        "valor": 20,
        "ciclista": 3
    },
    {
        "id": 8,
        "status": "Pendente",
        "horaSolicitacao": "2023-11-15T12:20:00.000Z",
        "horaFinalizacao ": null,
        "valor": 25,
        "ciclista": 4
    },
    {
        "id": 9,
        "status": "Concluída",
        "horaSolicitacao": "2023-11-15T14:50:00.000Z",
        "horaFinalizacao ": "2023-11-15T15:05:30.000Z",
        "valor": 30,
        "ciclista": 5
    },
    {
        "id": 10,
        "status": "Pendente",
        "horaSolicitacao": "2023-11-15T16:40:00.000Z",
        "horaFinalizacao ": null,
        "valor": 22,
        "ciclista": 6
    },
];


module.exports = filaCobrancasBd