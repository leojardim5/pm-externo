const cartoesModel = [{
        "IdDoCiclista": 1,
        "nomeTitular": "João Silva",
        "numero": "1111222233334444",
        "validade": "12/25",
        "cvv": "123"
    },
    {
        "IdDoCiclista": 2,
        "nomeTitular": "Maria Oliveira",
        "numero": "5555666677778888",
        "validade": "09/24",
        "cvv": "456"
    },
    {
        "IdDoCiclista": 3,
        "nomeTitular": "Carlos Gonzalez",
        "numero": "9999888877776666",
        "validade": "10/23",
        "cvv": "789"
    },
    {
        "IdDoCiclista": 4,
        "nomeTitular": "Aya Takahashi",
        "numero": "1234987654325678",
        "validade": "09/24",
        "cvv": "321"
    },
    {
        "IdDoCiclista": 5,
        "nomeTitular": "Luisa Hernandez",
        "numero": "7777666655554444",
        "validade": "05/24",
        "cvv": "654"
    },
    {
        "IdDoCiclista": 6,
        "nomeTitular": "André Santos",
        "numero": "1122334455667788",
        "validade": "09/23",
        "cvv": "987"
    },
]


module.exports = cartoesModel