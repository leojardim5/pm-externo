const exemplosGenericos = [{
        "id": 1,
        "status": "Ativo",
        "nome": "João Silva",
        "nascimento": "1990-05-15",
        "cpf": "12345678901",
        "passaporte": {
            "numero": "P123456",
            "validade": "2025-01-01",
            "pais": "BR"
        },
        "nacionalidade": "Brasileiro",
        "email": "joao@example.com",
        "urlFotoDocumento": "https://example.com/joao-documento.jpg"
    },
    {
        "cpf": "04029436486",
        "email": "leojardim5@gmail.com",
        "id": "c9e47bde-6c60-4b6c-91e3-ed253c47b372",
        "nacionalidade": "Brasileira",
        "nascimento": "2000-11-11",
        "nome": "Leonardo Jardim",
        "passaporte": {
            "numero": "123456",
            "pais": "BR",
            "validade": "2023-11-08"
        },
        "urlFotoDocumento": "url.com"
    },
    {
        "id": 3,
        "status": "Ativo",
        "nome": "Carlos Gonzalez",
        "nascimento": "1988-02-28",
        "cpf": "55566677788",
        "passaporte": {
            "numero": "P777888",
            "validade": "2023-10-01",
            "pais": "ES"
        },
        "nacionalidade": "Espanhol",
        "email": "carlos@example.com",
        "urlFotoDocumento": "https://example.com/carlos-documento.jpg"
    },
    {
        "id": 4,
        "status": "Ativo",
        "nome": "Aya Takahashi",
        "nascimento": "1995-11-03",
        "cpf": "12398745632",
        "passaporte": {
            "numero": "P987654",
            "validade": "2024-09-15",
            "pais": "JP"
        },
        "nacionalidade": "Japonesa",
        "email": "aya@example.com",
        "urlFotoDocumento": "https://example.com/aya-documento.jpg"
    },
    {
        "id": 5,
        "status": "Ativo",
        "nome": "Luisa Hernandez",
        "nascimento": "1992-04-10",
        "cpf": "45678912300",
        "passaporte": {
            "numero": "P654789",
            "validade": "2024-05-01",
            "pais": "MX"
        },
        "nacionalidade": "Mexicana",
        "email": "luisa@example.com",
        "urlFotoDocumento": "https://example.com/luisa-documento.jpg"
    },
    {
        "id": 6,
        "status": "Ativo",
        "nome": "André Santos",
        "nascimento": "1987-12-18",
        "cpf": "78945612311",
        "passaporte": {
            "numero": "P112233",
            "validade": "2023-09-01",
            "pais": "BR"
        },
        "nacionalidade": "Brasileiro",
        "email": "andre@example.com",
        "urlFotoDocumento": "https://example.com/andre-documento.jpg"
    },
];




module.exports = exemplosGenericos